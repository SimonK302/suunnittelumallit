package teht3;

public interface Component {
	public double calculatePrize();
	public void addComponent(Component component);
}
