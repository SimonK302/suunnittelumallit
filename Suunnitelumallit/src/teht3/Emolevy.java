package teht3;

import java.util.ArrayList;
import java.util.List;

public class Emolevy implements Component{
	double prize;

	List<Component> componentList = new ArrayList<Component>();

	public Emolevy(double prize){
		this.prize = prize;
	}

	@Override
	public double calculatePrize() {
		double sum = prize;
		for(Component c: componentList){
			sum = sum+c.calculatePrize();
		}
		return sum;
	}

	@Override
	public void addComponent(Component component) {
		// TODO Auto-generated method stub
		componentList.add(component);
	}

}
