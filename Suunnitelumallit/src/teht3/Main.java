package teht3;

public class Main {
	public static void main(String[] args){
		Component emolevy = new Emolevy(50.0);
		Component kotelo = new Kotelo(80.0);
		Component muistipiiri = new Muistipiiri(60.0);
		Component naytonohjain = new Naytonohjain(100.0);
		Component prosessori = new Prosessori(70.0);
		Component verkkokortti = new Verkkokortti(50.0);

		emolevy.addComponent(muistipiiri);
		emolevy.addComponent(naytonohjain);
		emolevy.addComponent(prosessori);
		emolevy.addComponent(verkkokortti);
		kotelo.addComponent(emolevy);

		System.out.println(kotelo.calculatePrize());
	}
}
