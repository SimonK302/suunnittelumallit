package teht21;

public class CPU {
	public long position;

	public void freeze() {
		System.out.println("Waiting for a reply");
	}

	public void jump(long position) {
		this.position = position++;
	}

	public void execute() {
		System.out.println("Booted succesfully");
	}
}
