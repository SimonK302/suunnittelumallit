package teht6;

public class Main {

	public static void main(String[] args){
		Pizza kinkkuPizza = new Kinkku(new Tomaatti(new Mozzarella(new Pohja())));
		Pizza salamiPizza = new Salami(new Tomaatti(new Mozzarella(new Pohja())));
		Pizza vegePizza = new Tomaatti(new Mozzarella(new Pohja()));

		System.out.println("Menu");
		System.out.println("Kinkkupizza: " + kinkkuPizza.getName() + " " + kinkkuPizza.getPrize() + "e");
		System.out.println("Salamipizza: " + salamiPizza.getName() + " " + salamiPizza.getPrize() + "e");
		System.out.println("Vegepizza: " + vegePizza.getName() + " " + vegePizza.getPrize() + "e");

	}

}
