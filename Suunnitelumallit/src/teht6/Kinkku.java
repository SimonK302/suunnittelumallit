package teht6;

public class Kinkku extends PizzaDecorator{
	private double prize = 1.0;

	public Kinkku(Pizza pizzaToBeDecorated) {
		super(pizzaToBeDecorated);
		// TODO Auto-generated constructor stub
	}

	public double getPrize(){
		return pizzaToBeDecorated.getPrize()+this.prize;
	}

	public String getName(){
		return super.getName() + ", kinkku";
	}

}
