package teht6;

public class Salami extends PizzaDecorator{
	private double prize = 1.0;

	public Salami(Pizza pizzaToBeDecorated) {
		super(pizzaToBeDecorated);
		// TODO Auto-generated constructor stub
	}

	public double getPrize(){
		return pizzaToBeDecorated.getPrize()+this.prize;
	}

	public String getName(){
		return super.getName() + ", salami";
	}

}
