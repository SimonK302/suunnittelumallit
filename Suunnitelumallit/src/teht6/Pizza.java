package teht6;

public interface Pizza {
	public double getPrize();
	public String getName();
}
