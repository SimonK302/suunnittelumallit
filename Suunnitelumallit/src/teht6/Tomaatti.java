package teht6;

public class Tomaatti extends PizzaDecorator{
	private double prize = 0.8;
	public Tomaatti(Pizza pizzaToBeDecorated) {
		super(pizzaToBeDecorated);
	}
	public double getPrize(){
		return pizzaToBeDecorated.getPrize()+this.prize;
	}

	public String getName(){
		return super.getName() + ", tomaatti";
	}

}
