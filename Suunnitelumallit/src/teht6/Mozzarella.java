package teht6;

public class Mozzarella extends PizzaDecorator{
	private double prize = 0.5;

	public Mozzarella(Pizza pizzaToBeDecorated){
		super(pizzaToBeDecorated);
	}

	public double getPrize(){
		return pizzaToBeDecorated.getPrize()+this.prize;
	}

	public String getName(){
		return super.getName() + ", mozzarella";
	}
}
