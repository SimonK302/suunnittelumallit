package teht6;

public abstract class PizzaDecorator implements Pizza{
	protected Pizza pizzaToBeDecorated;

	public PizzaDecorator(Pizza pizzaToBeDecorated){
		this.pizzaToBeDecorated = pizzaToBeDecorated;
	}

	public double getPrize(){
		return pizzaToBeDecorated.getPrize();
	}

	public String getName(){
		return pizzaToBeDecorated.getName();
	}
}
