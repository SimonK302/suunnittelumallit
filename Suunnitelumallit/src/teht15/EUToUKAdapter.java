package teht15;

public class EUToUKAdapter implements UKPlug{
	private EUPlug plug;

	public EUToUKAdapter(EUPlug plug){
		this.plug = plug;
	}
	@Override
	public void provideElectricity() {
		plug.giveElectricity();

	}

}
