package teht4;

public class Main {
	public static void main(String[] args) {
		ClockTimer ct = new ClockTimer();
		DigitalClock dc = new DigitalClock(ct);

		for (int i = 0; i < 10000; i++) {
			ct.tick();
		}
	}
}
