package teht4;

public class ClockTimer extends Subject {
	public int h,min,s;

	public int getHour(){
		return h;
	}

	public int getMinute(){
		return min;
	}

	public int getSecond(){
		return s;
	}

	public void tick(){
		s++;
		if(s>59){
			s=0;
			min++;
			if(min>59){
				min=0;
				h++;
				if(h>23){
					h=0;
				}
			}
		}
		tell();
	}
}
