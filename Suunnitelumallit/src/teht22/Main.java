package teht22;

public class Main {
	public static void main(String[] args){
		Screen screen = new Screen();
		Command switchUp = new FlipUpCommand(screen);
		Command switchDown = new FlipDownCommand(screen);
		WallButton1 button1 = new WallButton1(switchUp);
		WallButton1 button2 = new WallButton1(switchDown);

		button1.push();
		button2.push();
	}
}
