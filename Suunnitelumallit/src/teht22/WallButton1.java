package teht22;

public class WallButton1 {
	Command cmd;

	public WallButton1(Command cmd) {
		this.cmd = cmd;
	}

	public void push() {
		cmd.execute();
	}
}
