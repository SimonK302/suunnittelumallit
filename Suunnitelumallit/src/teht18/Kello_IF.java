package teht18;

public interface Kello_IF {
	public abstract void setTime(int h, int m, int s);
	public abstract String getTime();
	public abstract Kello clone();
}
