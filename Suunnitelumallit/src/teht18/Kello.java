package teht18;

public class Kello implements Kello_IF, Cloneable{
	private Viisari_IF sek, min, h;


	public Kello(){
		sek = new SViisari();
		min = new MViisari();
		h = new HViisari();
	}

	@Override
	public void setTime(int h, int m, int s) {
		this.h.setTime(h);
		this.min.setTime(m);
		this.sek.setTime(s);


	}

	@Override
	public String getTime() {
		String time = this.h.getTime() + ":" + this.min.getTime()+":"+this.sek.getTime();
		return time;
	}

	@Override
	public Kello clone(){
		Kello c = null;
		try{
			c = (Kello)super.clone();
			c.h = (HViisari)h.clone();
			c.min = (MViisari)min.clone();
			c.sek = (SViisari)sek.clone();
		}catch (CloneNotSupportedException e) {}
		return c;

	}

}
