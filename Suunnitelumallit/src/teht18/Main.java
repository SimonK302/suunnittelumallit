package teht18;

public class Main {

	public static void main(String[] args) {
		Kello_IF kello = new Kello();
		kello.setTime(15, 7, 45);
		System.out.println(kello.getTime());

		Kello_IF klooni = kello.clone();
		System.out.println(klooni.getTime());

//		kello.setTime(15, 24, 00);
		klooni.setTime(15, 26, 00);
		
		System.out.println(kello.getTime());
		System.out.println(klooni.getTime());
	}
}
