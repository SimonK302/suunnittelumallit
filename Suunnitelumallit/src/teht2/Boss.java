package teht2;

public class Boss implements VaateTehdas{

	@Override
	public Housut createHousut() {
		return new BHousut();
	}

	@Override
	public Kengat createKengat() {
		return new BKengat();
	}

	@Override
	public Lippis createLippis() {
		return new BLippis();
	}

	@Override
	public Paita createPaita() {
		return new BPaita();
	}

}
