package teht2;

public class Jasper{
	VaateTehdas vaateTehdas = null;

	Housut housut = null;
	Kengat kengat = null;
	Lippis lippis = null;
	Paita paita = null;

	public Jasper(VaateTehdas vt){
		this.vaateTehdas = vt;
	}


	public void housut(){
		this.housut = vaateTehdas.createHousut();
	}

	public void kengat(){
		this.kengat = vaateTehdas.createKengat();
	}

	public void lippis(){
		this.lippis = vaateTehdas.createLippis();
	}

	public void paita(){
		this.paita = vaateTehdas.createPaita();
	}

	public void pukeudu(){
		housut();
		kengat();
		lippis();
		paita();
		print();
	}

	public void print(){
		System.out.println("Olen Jasperi java-koodari");
		System.out.println("Puen jalkaani " + housut.toString());
		System.out.println("Puen päälleni " + paita.toString());
		System.out.println("Laitan jalkaani " + kengat.toString());
		System.out.println("Pistän päähäni " + lippis.toString());
	}


}
