package teht2;

public class Adidas implements VaateTehdas{

	@Override
	public Housut createHousut() {
		return new AHousut();
	}

	@Override
	public Kengat createKengat() {
		return new AKengat();
	}

	@Override
	public Lippis createLippis() {
		return new ALippis();
	}

	@Override
	public Paita createPaita() {
		return new APaita();
	}

}
