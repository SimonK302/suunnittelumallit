package teht2;

public interface VaateTehdas {
	public abstract Housut createHousut();
	public abstract Kengat createKengat();
	public abstract Lippis createLippis();
	public abstract Paita createPaita();
}
